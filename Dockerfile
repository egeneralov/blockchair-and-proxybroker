FROM python:3.7.5

WORKDIR /app
ADD requirements.txt /app/
RUN pip install --prefer-binary -U --no-cache-dir -r requirements.txt

ADD . .

CMD python3 app.py
