import urllib
import os
import subprocess
import json
from flask import Flask, Response, request
from requests import get
from threading import Thread
import random

app = Flask('__main__')
SITE_NAME = 'https://api.blockchair.com/'

PROXY_LIST = []

try:
  with open("http.txt") as f:
    raw = f.read().split("\n")
    i = [ i.split(":") for i in raw ]
    [ PROXY_LIST.append(i) for i in i if i != [''] ]
    print(PROXY_LIST)
except:
  pass

def __get_proxy__(count = 1):
  test = subprocess.check_output(f"proxybroker find --lvl High --types HTTP -s -f json -l {count}".split()).decode()
  test = json.loads(test)
  return [
    (i["host"], i["port"])
    for i in test
  ]

def get_raw_proxy():
  if len(PROXY_LIST) == 0:
    test = subprocess.check_output("proxybroker find --lvl High --types HTTP -s -f json -l 1".split()).decode()
    test = json.loads(test)
    return (test[0]["host"], test[0]["port"])
  else:
    return random.choice(PROXY_LIST)


class ProxyFinderThread(Thread):
  def __init__(self):
    Thread.__init__(self)
  def run(self):
    while True:
      if len(PROXY_LIST) < 10:
        count = 2
      else:
        count = 20
      [
        PROXY_LIST.append(i)
        for i in __get_proxy__(count)
        if i not in PROXY_LIST
      ]
      print(f"len(PROXY_LIST) = {len(PROXY_LIST)}")

if os.environ.get("AUTO"):
  ProxyFinderThread().start()


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def proxy(path):
  path = path + "?" + '&'.join(["{}={}".format(k, v) for k, v in request.args.to_dict().items()])
  p = get_raw_proxy()
  print(f'{p} {SITE_NAME}{path}')
  r = get(
    f'{SITE_NAME}{path}',
    proxies = {
      "http": "http://{}:{}".format(
        p[0], p[1]
      )
    },
    headers = {
      "Host": "api.blockchair.com",
      "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:71.0) Gecko/20100101 Firefox/71.0",
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
      "Accept-Language": "ru,en-US;q=0.7,en;q=0.3",
      "Accept-Encoding": "gzip, deflate, br",
      "DNT": "1",
      "Connection": "keep-alive",
      "Upgrade-Insecure-Requests": "1"
    }
  )
  if r.status_code != 200:
    try:
      PROXY_LIST.remove(p)
    except:
      pass
  return Response(r.text, headers = {"Content-Type": "application/json"}, mimetype='application/json'), r.status_code

app.run(host='0.0.0.0', port=8999)

